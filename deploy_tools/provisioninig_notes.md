Provisioning a new site
=======================

## Required packages:

* nginx 
* Python 3.6
* virtualenv + pip
* Git

eg, on Ubuntu

        sudo add-apt-repository ppa:fkrull/deadsnakes
        sudo apt-get install nginx git python3.6 python3.6-venv
        
 ## Nginx Virtual Host config
 
 * see nginx.tempalate.conf
 * replace SITENAME with, e.g., yoursitename.com
 
 ## Systemd service
 
 * see gunicorn-systemd.template.service
 * replace SITENAME with, e.g., yoursitename.com
 
 ## Folder structure
 
 Assume we have a user account at /home/username

    